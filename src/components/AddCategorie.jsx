// RAFCE
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import swal from 'sweetalert'


const AddCategorie = ({ categories }) => {
    const [inputVal, setInputVal] = useState('')

    const inputChange = e => setInputVal( e.target.value )
    const formSubmit = e => {
        e.preventDefault()
        if( inputVal.trim().length > 2 ){
            categories( calb => [ inputVal  ,...calb ])
            swal("Categorie Added!", "", "success");
            setInputVal('')
        }else{
            swal("Not Work!", "", "error");
        }
    }
    return (
        <form onSubmit={ formSubmit }>
            <div className="uk-margin uk-width-expand">
                <input
                className="uk-input uk-form-width-medium uk-width-expand" 
                type="text"
                placeholder="Add Categorie..." 
                value={inputVal}
                onChange={ inputChange } 
                />
            </div>
            <button className="uk-button uk-button-secondary" onClick={formSubmit} >Add Categorie</button>
        </form>

    )
}

AddCategorie.propTypes = {
     categories: PropTypes.func.isRequired
}

export default AddCategorie
