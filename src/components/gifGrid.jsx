import React, { useState , useEffect } from "react";
import GifGridItem from './gifGridItem'
import { getGifs } from "../helpers/getGifs";

export const GifGrid = ({ categorie }) => {
     
    //const [count, setCount] = useState(0)
    const [images, setImages] = useState([])
     
    useEffect( () => {
        getGifs( categorie )
        .then( img => setImages(img) )
    }, [categorie] ) 
    
    //getGifs();
    
    return (
        <>
            <h3> {categorie} </h3>
            <ol className="grid-cards">
                {
                    images.map( ele => <GifGridItem key={ele.id} dataChild={ele} />)
                }
            </ol>
        </>
    );
};
