import React from 'react'

const GifGridItem = ({ dataChild: { id, title, image: { url } } }) => {
    return (
        <div className="uk-card uk-card-default animate__animated animate__fadeIn">
            <div className="uk-card-media-top">
                <img src={url} alt={title} />
            </div>
            <div className="uk-card-body">
             <h3 className="uk-card-title">{ title }</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
            </div>
        </div>
    )
}

export default GifGridItem
