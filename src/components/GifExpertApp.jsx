import React, { useState } from 'react'
import AddCategorie from './AddCategorie'
import { GifGrid } from './gifGrid'


const GifExpertApp = () => {
       
        //const categories = [ 'One Piece' , 'Dragon Ball' , 'Deadpool'  ]
        const [categories, setCategorie] = useState(['One Piece'])
        
        
        console.log(categories)
        return (
            <>
            
            <h2 className="uk-margin-small-top uk-text-normal">GifExpertApp</h2>
            <AddCategorie categories={setCategorie} />
            <hr/>

             <ol className="uk-padding-small">
                 {
                     categories.map( (ele,index) => <GifGrid categorie={ele} key={index} />  )
                     //categories.map( (ele,index) => <li>ele</li>  )
                 }
             </ol>

            </>
        )
}

export default GifExpertApp
