import React from 'react';
import './App.css';
import GifExpertApp from './components/GifExpertApp';

function App() {
  return (
    <div className="container">
     <GifExpertApp/>
    </div>
  );
}

export default App;
