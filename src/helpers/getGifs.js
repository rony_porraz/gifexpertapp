export const getGifs = async (categorie) => {
    console.log(categorie)
    const URL = `https://api.giphy.com/v1/gifs/search?limit=10&q=${ categorie }&api_key=f31hfBocGOln2mBm7GVNNWRDaPrp4swE`;
    const resp = await fetch(URL);
    const { data } = await resp.json();
    const gifs = data.map((ele) => {
        return {
            id: ele.id,
            title: ele.title,
            image: ele.images.original,
        };
    });
    return gifs
};
